Asynchronous_Final_Project
============
We used coffee script, sass and pug for transpiling

How to setup and run the server
---------------------
First, run the command:
`npm install`

Then you can start the server:
`npm start`

Populate the database
---------------------
To populate the database, run the following command
`./bin/populatedb.sh`

It will populate the database with 2 users:

username : `user1`
password : `azerty`

and

username : `user2`
password : `azerty`

You will also have some metrics registered