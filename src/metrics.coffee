module.exports = (db) ->
  # get(username, callback)
  # Get metrics 
  # - username: metric's username 
  # - callback: the callback function, callback(err, data)
  get: (username, callback) ->
    metrics = []
    rs = db.createReadStream
      gte: "metrics:#{username}:" 
      lte: "metrics:#{username}:9"
    rs.on 'data', (data) ->
      metrics.push(data)
    rs.on 'error', () -> 
      console.log 'error'
    rs.on 'close', ->
      callback null, metrics
  
  #get(callback)
  #Get all the metrics
  # - callback: the callback function, callback(err,data)
  getAll: (callback) ->
    metrics = []
    rs = db.createReadStream
      lt: "users:"
    rs.on 'data', (data) ->
      metrics.push(data)
    rs.on 'error', () -> 
      console.log 'error'
    rs.on 'close', ->
      callback null, metrics

  # save(username, metrics, callback)
  # Save given metrics 
  # - username: metric username
  # - metrics: an array of { timestamp, value }
  # - callback: the callback function
  save: (username, body, callback) ->
    ws = db.createWriteStream()
    ws.on 'error', (err) ->
      console.log err 
    ws.on 'close', callback 
    ws.write 
      key: "metrics:#{username}:#{body.timestamp}"
      value: body.value
    ws.end()

 