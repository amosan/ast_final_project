bcrypt = require 'bcrypt-nodejs'

module.exports = (db) ->
  # auth(username, password, callback)
  # Check auth user 
  # - username: user's username
  # - password: user's password
  # - callback: the callback function, callback(err, data)
  auth: (username, password, callback) ->
    users = undefined
    rs = db.createReadStream
      gte: "users:#{username}"
      lte: "users:#{username}"
    rs.on 'data', (data) ->
      if bcrypt.compareSync(password, data.value)
        users = data
    rs.on 'error', callback
    rs.on 'close', ->
      callback null, users

  # get(username, callback)
  # Get user 
  # - username: user's username
  # - callback: the callback function, callback(err, data)
  get: (username, callback) ->
    users = {}
    rs = db.createReadStream
      gte: "users:#{username}"
      lte: "users:#{username}"
    rs.on 'data', (data) ->
        users = data
    rs.on 'error', callback
    rs.on 'close', ->
      callback null, users
  
  getAll: (callback) ->
    users = []
    rs = db.createReadStream
      gte: "users:"
    rs.on 'data', (data) ->
      users.push(data)
    rs.on 'error', () -> 
      console.log 'error'
    rs.on 'close', ->
      callback null, users

  # save(username, password, callback)
  # Save username and not crypted password 
  # - username: user's username
  # - password: user's password
  # - callback: the callback function
  save: (username, password, callback) ->
    ws = db.createWriteStream()
    hashPassword = bcrypt.hashSync(password);
    ws.on 'error', (err) ->
      console.log err 
    ws.on 'close', callback
    ws.write
      key: "users:#{username}"
      value: hashPassword
    ws.end()