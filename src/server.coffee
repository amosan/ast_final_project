express = require 'express'
bodyparser = require 'body-parser'
session = require 'express-session'
LevelStore = require('level-session-store')(session)
level = require 'level'
levelws = require 'level-ws'

db = levelws level "#{__dirname}/../db"
metrics = require('./metrics')(db)
user = require('./user')(db)

app = express()

authCheck = (req, res, next) ->
  unless req.session.loggedIn == true
    res.redirect '/login'
  else
    next()

app.set 'views', "#{__dirname}/../views"
app.set 'view engine', 'pug'

app.use '/', express.static "#{__dirname}/../public"

app.use session
  secret: 'MyAppSecret'
  store: new LevelStore './db/sessions'
  resave: true
  saveUninitialized: true

app.use bodyparser.json()
app.use bodyparser.urlencoded()

app.get '/', authCheck, (req, res) ->
  res.render 'index'

app.get '/seeMetrics', authCheck, (req, res) ->
  res.render 'seeMetrics'

app.get '/login', (req, res) ->
  res.render 'login'

app.post '/login', (req, res) ->
  user.auth req.body.username, req.body.password, (err, data) ->
    return next err if err
    unless data
      res.redirect '/login'
    else
      req.session.loggedIn = true
      req.session.username = data.key.split(':')[1]
      res.redirect '/'

app.get '/signup', (req, res) ->
  res.render 'signup'

app.post '/signup', (req, res) ->
  user.save req.body.username, req.body.password, (err) ->
    throw next err if err 
    res.redirect '/login'

#Delete sessions Informations
app.get '/logout', (req, res) ->
  delete req.session.loggedIn
  delete req.session.username
  res.redirect '/login'

################### METRICS

#Supposed to get all metrics from a specified user
app.get '/metricsByUser', (req, res) -> 
  metrics.get req.session.username, (err, data) ->
    throw next err if err
    res.status(200).json data

#Get all metrics
app.get '/metrics/', (req, res) -> 
  metrics.getAll (err, data) ->
    throw next err if err
    res.status(200).json data

#Post a metrics
app.post '/metrics', (req, res) -> 
  metrics.save req.session.username, req.body, (err) ->
    throw next err if err 
    res.redirect '/'


#################### USERS #########

#Get all users
app.get '/users/', (req, res) ->
  user.getAll (err, data) ->
    throw next err if err
    res.status(200).json data

#This request get everything from the database
app.get '/get', (req, res) ->
  rs = db.createReadStream()
  result = []
  rs.on 'data', (data) ->
    result.push(data)
  rs.on 'error', () ->
    console.log 'error'
  rs.on 'close', ->
    res.status(200).json result


############################### Populate the Database ##################

app.get '/populateDatabase', (req, res) ->
  data = [{"key":"metrics:users:Seco_:2018-12-14 15:41 UTC","value":"23"},
          {"key":"metrics:users:test_:2001-12-30 00:00 UTC","value":"277"},
          {"key":"metrics:users:test_:2005-11-11 00:01 UTC","value":"2"},
          {"key":"users:Seco_","value":"test"},{"key":"users:test_","value":"test"}]#On this line you have the 2 users by default 'test' and 'A' (You don't care about the '_' it is used in back to know the end of a username)
  ws = db.createWriteStream()
  ws.on 'error', (err) ->
    console.log err 
  ws.on 'close', (err) ->
    throw next err if err
    res.status(200).send 'Database populated, check README.MD if you don\'t remember the users'
  for da in data
    ws.write 
      key: da.key
      value: da.value
  ws.end()

########################################################################


app.set 'port', 8888

app.listen app.get('port'), () ->
  console.log "server listening on #{app.get 'port'}"
