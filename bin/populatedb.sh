#!/usr/bin/env coffee

level = require 'level'
levelws = require 'level-ws'

db = levelws level "#{__dirname}/../db"
metrics = require('../src/metrics')(db)
user = require('../src/user')(db)

metrics.save 'user1', {timestamp:"2017-12-18", value:"12"}, (err) ->
  throw err if err
  console.log 'Metrics1 for user 1 saved'

metrics.save 'user1', {timestamp:"2017-12-23", value:"32"}, (err) ->
  throw err if err
  console.log 'Metrics2 for user 1 saved'

metrics.save 'user1', {timestamp:"2017-12-24", value:"21"}, (err) ->
  throw err if err
  console.log 'Metrics3 for user 1 saved'

metrics.save 'user2', {timestamp:"2017-12-01", value:"90"}, (err) ->
  throw err if err
  console.log 'Metrics1 for user 2 saved'

metrics.save 'user2', {timestamp:"2017-12-23", value:"100"}, (err) ->
  throw err if err
  console.log 'Metrics2 for user 2 saved'

metrics.save 'user2', {timestamp:"2017-11-19", value:"120"}, (err) ->
  throw err if err
  console.log 'Metrics3 for user 2 saved'


user.save 'user1', 'azerty', (err) ->
  throw err if err
  console.log 'user1 created'

user.save 'user2', 'azerty', (err) ->
  throw err if err
  console.log 'user2 created'