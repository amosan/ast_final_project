#!/bin/bash

set -e # Set bash flag to interrupt if any command fails

compileCoffee() {
  echo "Compiling coffee files"
  node_modules/.bin/coffee --compile --output lib src
}

compilePug() {
  echo "Compile pug files"
  node_modules/.bin/pug views --out public/html --pretty
}

compileSass() {
  echo "Compiling Sass files"
  node_modules/.bin/node-sass --output public/css views/sass 
}

main() {  
  case "$1" in 
  coffee)
    compileCoffee
    ;;
    
  pug)
    compilePug
    ;;
    
  Sass)
    compileSass
    ;;
    
  *) 
    if [[ ! -z "$0" ]]; then 
      echo "Compiling everything"
      compileCoffee
      compilePug
      compileSass
    fi;
    ;;
  esac

  echo "Done"
}

main $1